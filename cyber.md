class: middle, center

# Cyberattacks 

---

class: top, center

# Cyberattacks

### ...what are they?
--

### ...who is behind them?
--

### ...do they affect us?

---

class: middle, center

## .center[What are Cyberattacks?]

---
class: top, left
## .center[What are Cyberattacks?]

### Definitions
???
1. yourdictionary.com/cyberattack
2. cambridge dictionary
3. merriam-webster dictionary
--

*An attack or intrusion by means of a computer network such as the Internet, often for the purpose of spying*

--

*an illegal attempt to harm someone's computer system or the information on it, using the internet*

--

*an attempt to gain illegal access to a computer or computer system for the purpose of causing damage or harm*

---

class: top, left

## .center[What are Cyberattacks?]
### Types
--
 
* DDoS (Distributed Denial of Service)
--

* Phishing
--

* MITM (man-in-the-middle)
--

* Ransomware
--

* Destructive weapons
--

* Takeover
--

* Crypto Jacker
--

* Intelligence operations
--

* Criminal Prosecution
--

* ...

---
class: middle, center
## .center[Who is behind them?]

---
class: top, left

## .center[Who is behind them?]
### Actors
--

* Thieves
???
* Thieves: Banking information, credit cards
* Hacktivists: DDoS, Takeover of prominent law enforcement websites (FBI, CIA)
* Mad kids: see mirai
* Law Enforcement: Police, Intelligence Services (Verfassungsschutz)
  ZITis: Zentrale Stelle für Informationstechnik im Sicherheitsbereich (Go-To partner for German police and intel. agencies

--

* Hacktivists *(LulzSec)*
--

* Mad kids *(Anna-Senpai)*
--

* Law Enforcement *(ZITiS)*
--

* Nation-state actors
  * 3-Letter-Agencies *(BND, CIA, NSA)*
  * External Groups *(Equation Group)*


---
class: middle, center
# Examples

---
class: top, left
## .center[Example 0: Mirai]
* Type: DDoS
* Actor: Mad kid
* Attribution: Anna-Senpai
* What happened?
  - IoT Botnet
  - DDoS against Minecraft servers
  - Partial internet outage for a few days
---
class: top, left
## .center[Example 0: Mirai]

### Takeaways
  - The 'S' in IoT stands for *Security*
  - Don't attack Brian Krebs

---
class: top, left
## .center[Example 1: Bundeshack]
* Type: Intelligence
* Actor: *Ivan*
* Attribution: Russia*
* What happened?
  - Bug in old version of ILIAS
  - BSI let it continue *'isolated'* to collect evidence (#SichereNetze)
  - Command and control via email attachments (Microsoft Outlook)

\* *per default* 

???
in the German Federal Foreign Office (Auswärtiges Amt)

---
class: top, left
## .center[Example 1: Bundeshack]
### Takeaways
  - Update computers
  - Diversify software stack


---
class: top, left
## .center[Example 2: WannaCry]
* Type: Ransomware
* Actor: Lazarus*?*
* Attribution: North Korea
* What happened?
  - NSA tools got leaked
  - Patch was available for two months
  - Bitcoin address generation went wrong

---
class: top, left
## .center[Example 2: WannaCry]
### Takeaways
  - Write a proper bitcoin address generator
  - Governments collecting malware for their own use are a danger to everyone

???
Lazarus? 
EternalBlue: NSA exploit of SMB Bug in Windows; kept secret for over 5 years from MS; created by Equation Group; The Shadow Brokers stole EG's tools -> made NSA disclose to MS; 1 Month after patches, Shadow Brokers release Source

---
class: top, left
## .center[Example 3: *Shamoon 2.0*]
* Type: Destructive Weapon
* Actor: Darius*
* Attribution: Iran
* What happened?
  - Saudi Arabian petrochemical plant control was targeted
  - Attack took place in August 2017
  - Supposed to create an explosion but a bug save everyone's lifes

\* *Name was changed for privacy reasons (GDPR)*
???
Shamoon: Codename of previous version; Hackers targeted a common control system (worldwide); Software cased shutdown on accident;Previously: Wipe data and replace with a burning american flag

---
class: top, left
## .center[Example 3: *Shamoon 2.0*]
### Takeaways
  - Cyberattacking kills *(if executed correctly)*

---
class: middle, center

# Any questions?
